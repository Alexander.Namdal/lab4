package no.uib.inf101.gridview;


import no.uib.inf101.colorgrid.CellPosition;
import no.uib.inf101.colorgrid.GridDimension;

import java.awt.geom.Rectangle2D;

public class CellPositionToPixelConverter {
    private Rectangle2D box;
    private GridDimension gd;
    private double margin;


    public CellPositionToPixelConverter(Rectangle2D box, GridDimension gd, double margin) {
        this.box = box;
        this.gd = gd;
        this.margin = margin;
    }

    public Rectangle2D.Double getBoundsForCell(CellPosition cp){

        int rows = gd.rows();
        int cols = gd.cols();
        double cellW = (box.getWidth() - (margin * (cols + 1))) / cols;
        double cellH = (box.getHeight() - (margin * (rows + 1))) / rows;
        double x = box.getX() + cp.col() * (cellW + margin) + margin;
        double y = box.getY() + cp.row() * (cellH  + margin) + margin;
        return new Rectangle2D.Double(x,y,cellW,cellH);
    }
}
