package no.uib.inf101.gridview;

import no.uib.inf101.colorgrid.*;

import javax.swing.*;
import java.awt.*;
import java.awt.geom.Rectangle2D;

public class GridView extends JPanel {
    private static final double OUTERMARGIN = 30;
    private static final Color MARGINCOLOR = Color.LIGHT_GRAY;
    private IColorGrid colorGrid;
    private CellPositionToPixelConverter converter;
    private double margin = 10;

    public GridView(IColorGrid colorGrid) {

        this.colorGrid = (ColorGrid) colorGrid;
        this.setPreferredSize(new Dimension(400, 300));
    }

    @Override
    protected void paintComponent(Graphics g) {
        super.paintComponent(g);
        Graphics2D g2 = (Graphics2D) g;
        drawGrid(g2);
        // Assuming cellColors is accessible here or you have a method to obtain it
    }


        private void drawGrid(Graphics2D g2) {
        Rectangle2D outerBox = new Rectangle2D.Double(
                OUTERMARGIN,
                OUTERMARGIN,
                getWidth() - 2 * OUTERMARGIN,
                getHeight() - 2 * OUTERMARGIN);
        g2.setColor(MARGINCOLOR);
        g2.fill(outerBox);
        Rectangle2D gridBox = new Rectangle2D.Double(
                OUTERMARGIN,
                OUTERMARGIN,
                getWidth() - 2 * OUTERMARGIN,
                getHeight() - 2 * OUTERMARGIN);
        GridDimension gridDimension = new ColorGrid(colorGrid.rows(), colorGrid.cols());
        double cellMargin = 30;
        this.converter = new CellPositionToPixelConverter(gridBox, gridDimension, cellMargin);
        drawCells(g2, colorGrid, converter);
    }

    private static void drawCells(Graphics2D g2, CellColorCollection cellColors, CellPositionToPixelConverter converter) {
        for (CellColor cellColor : cellColors.getCells()) {
            CellPosition position = cellColor.cellPosition();
            Rectangle2D bounds = converter.getBoundsForCell(position);
            Color color = cellColor.color() != null ? cellColor.color() : Color.DARK_GRAY;
            g2.setColor(color);
            g2.fill(new Rectangle2D.Double(bounds.getX(), bounds.getY(), bounds.getWidth(), bounds.getHeight()));
        }
    }
}
