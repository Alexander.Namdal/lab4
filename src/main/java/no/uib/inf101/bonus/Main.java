package no.uib.inf101.bonus;

import javax.swing.*;

public class Main {
  public static void main(String[] args) {
    JFrame frame = new JFrame("A Beautiful Picture");
    frame.setDefaultCloseOperation(JFrame.EXIT_ON_CLOSE);
    frame.add(new BeautifulPicture());
    frame.setSize(800, 800);
    frame.setLocationRelativeTo(null); // Center the frame
    frame.setVisible(true);
  }
}
