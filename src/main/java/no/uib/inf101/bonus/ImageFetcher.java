package no.uib.inf101.bonus;

import java.awt.image.BufferedImage;
import java.io.IOException;
import java.net.URL;
import javax.imageio.ImageIO;

public class ImageFetcher {
    public static BufferedImage fetchImage(String imageUrl) {
        try {
            URL url = new URL(imageUrl); //Fetch URL and return image.
            return ImageIO.read(url);
        } catch (IOException e) {
            e.printStackTrace();
        }
        return null;
    }
}
