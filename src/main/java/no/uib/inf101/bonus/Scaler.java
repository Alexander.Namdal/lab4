package no.uib.inf101.bonus;
import java.awt.*;
import java.awt.geom.Ellipse2D;
import java.awt.geom.Rectangle2D;

public class Scaler {
    //I have only made methods for the shapes used: Polygon, circle & rectangle.
    private static double scaleX;
    private static double scaleY;

    public Scaler(int originalWidth, int originalHeight, int currentWidth, int currentHeight) {
        this.scaleX = currentWidth / (double) originalWidth;
        this.scaleY = currentHeight / (double) originalHeight;
    }



    public static Polygon scalePolygon(int[] xPoints, int[] yPoints) {
        int[] scaledXPoints = new int[xPoints.length];
        int[] scaledYPoints = new int[yPoints.length];

        for (int i = 0; i < xPoints.length; i++) {
            scaledXPoints[i] = (int) (xPoints[i] * scaleX);
            scaledYPoints[i] = (int) (yPoints[i] * scaleY);
        }

        return new Polygon(scaledXPoints, scaledYPoints, scaledXPoints.length);
    }



    public Ellipse2D scaleCircle(int centerX, int centerY, int radius) {
        int scaledCenterX = (int) (centerX * scaleX);
        int scaledCenterY = (int) (centerY * scaleY);
        int scaledRadius = (int) (radius * scaleX); // Assuming uniform scaling for circles

        return new Ellipse2D.Double(
                scaledCenterX - scaledRadius,
                scaledCenterY - scaledRadius,
                scaledRadius * 2,
                scaledRadius * 2
        );
    }



    public Rectangle2D scaleRectangle(int x, int y, int width, int height) {
        double scaledX = x * scaleX;
        double scaledY = y * scaleY;
        double scaledWidth = width * scaleX;
        double scaledHeight = height * scaleY;

        // Return a new Rectangle2D with the scaled dimensions
        return new Rectangle2D.Double(scaledX, scaledY, scaledWidth, scaledHeight);
    }
}
