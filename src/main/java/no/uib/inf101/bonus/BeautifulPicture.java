package no.uib.inf101.bonus;

import javax.swing.*;
import java.awt.*;
import java.awt.geom.Ellipse2D;
import java.awt.geom.Rectangle2D;
import java.awt.image.BufferedImage;
import java.nio.Buffer;
import java.util.Random;
import java.util.stream.DoubleStream;

public class BeautifulPicture extends JPanel {
    //Images:
    BufferedImage moonImage = ImageFetcher.fetchImage("https://i.imgur.com/sxIRhqk.png");
    BufferedImage astronautImage = ImageFetcher.fetchImage("https://www.uib.no/sites/w3.uib.no/files/styles/user_thumbnail/public/pictures/picture-393477-1638797650.jpg?itok=Zt_2_X8q");
    BufferedImage earthImage = ImageFetcher.fetchImage("https://upload.wikimedia.org/wikipedia/commons/thumb/2/22/Earth_Western_Hemisphere_transparent_background.png/1200px-Earth_Western_Hemisphere_transparent_background.png");
    BufferedImage saturnImage = ImageFetcher.fetchImage("https://upload.wikimedia.org/wikipedia/commons/thumb/4/43/Saturnx.png/1200px-Saturnx.png");
    BufferedImage shipHullImage = ImageFetcher.fetchImage("https://t3.ftcdn.net/jpg/01/02/95/50/360_F_102955012_YDQjFu619vEQnvTynByK7NESJQ5O44vS.jpg");
    BufferedImage backgroundNebulaeImage = ImageFetcher.fetchImage("https://i.imgur.com/2Xt3SY7.png");
    BufferedImage fireImage = ImageFetcher.fetchImage("https://live.staticflickr.com/5202/5194619353_8e772d53b1_c.jpg");
    BufferedImage satteliteImage = ImageFetcher.fetchImage("https://i.imgur.com/GZjQeMQ.png");
    BufferedImage deathstarImage = ImageFetcher.fetchImage(("https://i.imgur.com/8oBSASE.png"));
    BufferedImage cheesesignImage = ImageFetcher.fetchImage("https://i.imgur.com/7aHBMQO.png");
    @Override
    protected void paintComponent(Graphics g) {
        super.paintComponent(g);
        Graphics2D g2d = (Graphics2D) g;

        drawGradient(g2d);
        drawSaturn(g2d);
        drawStars(g2d);
        drawSign(g2d);
        drawSpaceship(g2d);
        drawPlanetEarth(g2d);
        drawMoon(g2d);
        drawDeathstar(g2d);

    }
    private void drawSpaceship(Graphics2D g2d) {
        Scaler scaler = new Scaler(800,800, getWidth(), getHeight());
        // Original points of the spaceship, created using Geogebra
        int[] XPoints = {460,420,380,340,320,310,300,280,300,320,340,
                320,360,380,340,350,370,380,380,380,420,450,460,};
        int[] YPoints = {300,310,340,380,380,380,390,410,420,400,380,
                400,440,420,460,480,460,450,440,420,380,340,300,};
        Polygon spaceship = scaler.scalePolygon(XPoints, YPoints);
        g2d.setColor(Color.GRAY);
        g2d.fill(spaceship);
        // Draw outlines
        g2d.setColor(Color.BLACK);
        ImageOverlay.fillShapeWithTexture(g2d, spaceship, shipHullImage);
        g2d.draw(spaceship);

        int[] XPointsBurn = {320,300,320,290,300,290,310,310,340,330,360,};
        int[] YPointsBurn = {400,430,420,450,450,470,460,470,440,460,440,};
        Polygon spaceshipBurn = scaler.scalePolygon(XPointsBurn, YPointsBurn);
        g2d.setColor(Color.RED);
        g2d.fill(spaceshipBurn);
        // Draw outlines
        ImageOverlay.fillShapeWithTexture(g2d, spaceshipBurn, fireImage);
        g2d.setColor(Color.BLACK);
        g2d.draw(spaceshipBurn);

        Ellipse2D circle = scaler.scaleCircle(420, 340, 10);
        ImageOverlay.fillShapeWithTexture(g2d, circle, astronautImage);
        g2d.fill(circle);
        g2d.setColor(Color.BLACK);
        g2d.draw(circle);



    }


    private void drawPlanetEarth(Graphics2D g2d){
        Scaler scaler = new Scaler(800,800, getWidth(), getHeight());
        Ellipse2D circle = scaler.scaleCircle(0, 800, 300);
        ImageOverlay.fillShapeWithTexture(g2d, circle, earthImage);
        //Draw satellite within this method as well...
        Ellipse2D circleSattelite = scaler.scaleCircle(40, 500, 10);
        ImageOverlay.fillShapeWithTexture(g2d, circleSattelite, satteliteImage);

    }
    private void drawGradient(Graphics2D g2d) {
        Scaler scaler = new Scaler(800,800, getWidth(), getHeight());
        Rectangle2D rectangle = scaler.scaleRectangle(-800,-800,1800, 1800);
        g2d.setPaint(new GradientPaint(0, 0, Color.BLACK, 0, getHeight(), Color.DARK_GRAY));
        g2d.fillRect(0, 0, getWidth(), getHeight());//Set a background gradient for the image, since it's transparent.
        ImageOverlay.fillShapeWithTexture(g2d, rectangle, backgroundNebulaeImage);
    }

    private void drawSaturn(Graphics2D g2d) {
        Scaler scaler = new Scaler(800,800, getWidth(), getHeight());
        Ellipse2D circle = scaler.scaleCircle(700, 700, 13);
        ImageOverlay.fillShapeWithTexture(g2d, circle, saturnImage);
    }

    private void drawSign(Graphics2D g2d) {
        Scaler scaler = new Scaler(800,800, getWidth(), getHeight());
        Ellipse2D circle = scaler.scaleCircle(450, 230, 35);
        ImageOverlay.fillShapeWithTexture(g2d, circle, cheesesignImage);
    }

    private void drawDeathstar(Graphics2D g2d) {
        Scaler scaler = new Scaler(800,800, getWidth(), getHeight());
        Ellipse2D circle = scaler.scaleCircle(140, 20, 10);
        ImageOverlay.fillShapeWithTexture(g2d, circle, deathstarImage);
    }

    private void drawMoon(Graphics2D g2d) {
        Scaler scaler = new Scaler(800,800, getWidth(), getHeight());
        Ellipse2D circle = scaler.scaleCircle(740, 60, 120);
        ImageOverlay.fillShapeWithTexture(g2d, circle, moonImage);
    }


    private void drawStars(Graphics2D g2d) {
        Random rand = new Random();
        g2d.setColor(Color.WHITE);
        for (int i = 0; i < 100; i++) { //Draw stars across the canvas.
            double starSize = rand.nextDouble(0.5, 4.0);
            int x = rand.nextInt(getWidth());
            int y = rand.nextInt(getHeight());
            g2d.fill(new Ellipse2D.Double(x, y, starSize, starSize));
        }
    }
}
