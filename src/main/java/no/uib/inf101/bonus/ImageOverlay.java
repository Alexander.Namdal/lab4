package no.uib.inf101.bonus;

import java.awt.*;
import java.awt.geom.Rectangle2D;
import java.awt.image.BufferedImage;

public class ImageOverlay {
    // Requires Graphics2D object to draw on, a shape to fill and an image

    public static void fillShapeWithTexture(Graphics2D g2d, Shape shape, BufferedImage image) {
        // Create a Rectangle2D from the bounds of the shape to define the texture area
        Rectangle2D bounds = shape.getBounds2D();
        // Create a TexturePaint from the image
        TexturePaint texturePaint = new TexturePaint(image, bounds);
        // Set the paint of the Graphics2D context to the TexturePaint
        g2d.setPaint(texturePaint);
        // Fill the shape with the texture
        g2d.fill(shape);
    }
}
