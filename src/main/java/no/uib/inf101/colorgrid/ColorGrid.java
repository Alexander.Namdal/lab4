package no.uib.inf101.colorgrid;

import java.awt.*;
import java.util.ArrayList;
import java.util.List;
public class ColorGrid implements IColorGrid {

    private Color[][] grid;
    private int cols;
    private int rows;

    public ColorGrid(int rows, int cols) {
        this.rows = rows;
        this.cols = cols;
        grid = new Color[rows][cols];
    }

    @Override
    public List<CellColor> getCells() {
        List<CellColor> cellList = new ArrayList<>();
        for (int row = 0; row < rows; row++) {
            for (int col = 0; col < cols; col++) {
                cellList.add(new CellColor(new CellPosition(row, col), grid[row][col]));
            }
        }
        return cellList;
    }

    @Override
    public int rows() {
        return rows;
    }

    @Override
    public int cols() {
        return cols;
    }

    @Override
    public Color get(CellPosition pos) {
        int row = pos.row();
        int col = pos.col();
        if (row < 0 || row >= rows || col < 0 || col >= cols) {
            throw new IndexOutOfBoundsException("Row or column index is out of bounds.");
        }
        return grid[row][col];
    }
    public void set(CellPosition pos, Color color) {
        int row = pos.row();
        int col = pos.col();
        if (row < 0 || row >= rows || col < 0 || col >= cols) {
            throw new IndexOutOfBoundsException("Row or column index is out of bounds.");
        }
        grid[row][col] = color;
    }
}
